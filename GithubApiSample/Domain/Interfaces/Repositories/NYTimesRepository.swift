

import Foundation
protocol NYTimesRepository
{
  
    func fetchMostPopularArticles(with FilterType: String, Period: Int, completion: @escaping (Result<PopularArticlesResponse>) -> Void)
}

