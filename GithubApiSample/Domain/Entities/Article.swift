import Foundation


// MARK: - Article
struct Article: Codable {
    
    let adx_keywords: String
    let Title : String
    let source: String
    let abstract: String
    let media : [Media]
    let published_date : String
    let byline : String
    let url : String
    enum CodingKeys: String, CodingKey {
       
        case Title = "title"
        case adx_keywords = "adx_keywords"
        case source = "source"
        case abstract = "abstract"
        case media = "media"
        case published_date = "published_date"
        case byline = "byline"
        case url = "url"
    }
}



// MARK: - Media
struct Media: Codable {
    let type, subtype, caption, copyright: String
    let approvedForSyndication: Int
    let mediaMetadata: [MediaMetadatum]

    enum CodingKeys: String, CodingKey {
        case type, subtype, caption, copyright
        case approvedForSyndication = "approved_for_syndication"
        case mediaMetadata = "media-metadata"
    }
}

// MARK: - MediaMetadatum
struct MediaMetadatum: Codable {
    let url: String
    let format: String
    let height, width: Int
}
