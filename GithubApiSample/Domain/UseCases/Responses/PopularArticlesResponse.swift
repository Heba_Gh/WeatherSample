import Foundation

// MARK: - Most Popular Articles Response
struct PopularArticlesResponse: Codable {

  let articles: [Article]?


  public init( articles : [Article]? ) {
    self.articles = articles
  }

}
