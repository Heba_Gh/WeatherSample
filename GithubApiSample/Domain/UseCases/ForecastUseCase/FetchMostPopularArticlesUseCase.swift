import Foundation
import RxSwift

protocol FetchMostPopularArticlesUseCase
{
  func execute(with FilterType : String , Period: Int ) -> Observable<Result<PopularArticlesResponse>>
}

final class DefaultFetchMostPopularArticlesUseCase : FetchMostPopularArticlesUseCase {
  
  private let defaultRepository: NYTimesRepository
  
  init(defaultRepository: NYTimesRepository)
  {
    self.defaultRepository = defaultRepository
  }
  
  func execute(with FilterType : String , Period : Int ) -> Observable<Result<PopularArticlesResponse>> {
    
    return Observable<Result<PopularArticlesResponse>>.create { observer in
      if !isOnline
      {
        observer.onNext(Result.failure(AppError(reason: .networkError, message: "Please check your internet connection")))
      }
      else{
        observer.onNext(Result.loading)
        
        self.defaultRepository.fetchMostPopularArticles(with: FilterType, Period: Period,  completion: { result in
          
          observer.onNext(result)
        })
      }
      return Disposables.create()
    }
  }
}


