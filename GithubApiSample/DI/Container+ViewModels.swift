import Foundation
import Swinject
import SwinjectAutoregistration

extension Container {

  func registerViewModels() {
    autoregister(ApiDataNetworkConfig.self, initializer: ApiDataNetworkConfig.init)
    autoregister(RemoteDataSource.self, initializer: RemoteDataSource.init)
    autoregister(NYTimesRepository.self, initializer: DefaultNYTimesRepository.init)
    autoregister(NYTimesViewModel.self, initializer: NYTimesViewModel.init)
  }
}
