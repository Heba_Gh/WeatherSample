import Foundation
import Swinject
import SwinjectAutoregistration

extension Container {
  
    func registerUseCases() {
        autoregister(FetchMostPopularArticlesUseCase.self, initializer: DefaultFetchMostPopularArticlesUseCase.init)
    }
}
