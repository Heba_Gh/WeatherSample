import Foundation

final class DefaultNYTimesRepository {
  private let config: ApiDataNetworkConfig
  private let remoteDataSource : RemoteDataSource
  
  init( config : ApiDataNetworkConfig , remoteDataSource : RemoteDataSource) {
    self.config = config
    self.remoteDataSource = remoteDataSource
  }
}

extension DefaultNYTimesRepository: NYTimesRepository {
  
  func fetchMostPopularArticles(with FilterType: String, Period: Int, completion: @escaping (Result<PopularArticlesResponse>) -> Void) {
    
    let requestParameter = MostPopularArticlesRequestValue()
    
      let endpoint = APIEndpoints.getMostArticlesEndPoint(with: requestParameter , FilterType: FilterType , Period : Period)
    do {
      let ( urlRequest ,  parameter) = try endpoint.urlRequest(with: config)
      remoteDataSource.sendRequest( urlRequest, param: parameter  , completion: {
        (result: Result<FetchMostPopularArticleResponse>) in
        switch result {
        case .success( let data , let code , let message):
          //  mapping PeriodicForcastsResponse to PeriodicForcasts to return just the needed data to the use case
          let periodicForcasts = PopularArticlesResponse(articles: data.results )
          
            completion(.success(data: periodicForcasts , code: code , message: message))
          break
        case .failure(let error):
          completion(.failure(error))
          break
        case .loading:
          completion(.loading)
          break
        }
      })
    }
    catch {
      completion(.failure(nil))
      
    }
    
  }
}
