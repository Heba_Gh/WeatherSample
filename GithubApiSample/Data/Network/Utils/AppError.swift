import Foundation

public struct AppError : Error , Equatable
{
    var reason : GetFailureReason?
    var message : String?
    init(reason :GetFailureReason , message : String? )
    {
        self.reason = reason
        self.message = message
    }
}

// MARK: - GetFailureReason
enum GetFailureReason: Int, Error
{
    case unAuthorized = 401
    case Forbidden = 403
    case notFound = 404
    case networkError = 700
    case urlGeneration = 701
    case unknownError = 702
}
