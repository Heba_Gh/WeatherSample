import Foundation

// MARK: - Set api request configuration (base url - headers)
public struct ApiDataNetworkConfig {
    public let baseURL: URL
    public let headers: [String: String]
 
     public init() {
        self.baseURL =  URL(string: "http://api.nytimes.com/svc/mostpopular/v2/")!
        self.headers = generateHeaders()
    }
}

// MARK: - Set api request header parameters 
func generateHeaders() ->[String:String] {
    var headers = [String:String]()
    let INPUT_DIVICE_TYPE = "DEVICE_TYPE"
    let INTPUT_ACCEPT = "Accept"
    headers[INPUT_DIVICE_TYPE] = DEVICE
    headers[INTPUT_ACCEPT] = "application/json"
    return headers
}
