import Foundation
import Alamofire

// MARK: - Remote Data Source
class RemoteDataSource {
  
  func sendRequest<T>(_ request: URLRequest , param : [String : Any] ,completion: @escaping  (Result<T>) -> Void) where T: Decodable {
    
    AF.request(request.url!.absoluteString, method: request.method!, parameters: param , headers: request.headers).responseJSON {
      (response) in
      
      switch response.result {
        
      case .success(_):
        do {
          let result = try JSONDecoder().decode(T.self, from: response.data!)
          completion(.success(data: result , code : response.response?.statusCode ?? 200 , message: ""))
          
        } catch {
          do {
            let result = try JSONDecoder().decode(FetchMostPopularArticleErrorResponse.self, from: response.data!)
            if let statusCode = result.code,
               let reason = GetFailureReason(rawValue: statusCode) {
              print(reason)
              let appError = AppError(reason: reason, message: result.message)
              completion(.failure(appError))
              
            }
          }
          catch{
            completion(.failure(nil))
          }
          
        }
      case .failure(_):
        if let statusCode = response.response?.statusCode,
           let reason = GetFailureReason(rawValue: statusCode) {
          print(reason)
          let appError = AppError(reason: reason, message: "unknown error")
          completion(.failure(appError))
          
        }else{
          completion(.failure(nil))
        }
      }
    }
  }
  
}


enum Result<T > {
  
  case success(data: T , code : Int , message : String)
  case failure(AppError?)
  case loading
}



