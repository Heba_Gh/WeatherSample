import Foundation

struct APIEndpoints {
    static func getMostArticlesEndPoint(with  periodicForecastRequestValue : MostPopularArticlesRequestValue , FilterType : String , Period: Int ) -> Endpoint {
        return Endpoint(path: "mostviewed/\(FilterType)/\(Period).json",
                        method: .get,
                        queryParametersEncodable: periodicForecastRequestValue)
    }
}



