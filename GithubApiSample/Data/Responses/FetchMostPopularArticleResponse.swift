import Foundation

// MARK: - popular articles Response
struct FetchMostPopularArticleResponse: Codable {
    let status: String
   
    let results: [Article]?
   
  
  enum CodingKeys: String, CodingKey {
      case status
     
      case results = "results"
  }
}
