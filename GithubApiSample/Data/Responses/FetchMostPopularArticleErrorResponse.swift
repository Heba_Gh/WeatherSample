import Foundation

// MARK: - Periodic Forcasts Error Response
struct FetchMostPopularArticleErrorResponse: Codable {
    let code : Int?
    let message: String
    enum CodingKeys: String, CodingKey {
      case message
      case code = "cod"
     
  }
}
