import Foundation

class DateFormats {

    static func changeDataFormat(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DATE = dateFormatter.date(from: date )
        if DATE != nil {
            dateFormatter.dateFormat = "ddd,MMM yy HH:mm"
            dateFormatter.timeZone = TimeZone.current
            let timeStamp = dateFormatter.string(from: DATE! )
            return timeStamp
        }
        return date
    }
    
    
}
