import UIKit

class MainCoordinator: Coordinator {
  
  var navigationController: UINavigationController
  static var shared : MainCoordinator?
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func push(destination: Destination) {
    
  }
    
    func pop() {
        navigationController.popViewController(animated: true)
    }
    
    func pushNYTimesPage()
    {
        let vc = NYTimesViewController(nibName: "NYTimesViewController", bundle: nil)
        vc.viewModel = AppDelegate.container.resolve(NYTimesViewModel.self)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    func pushArticleDetails(article:Article)
    {
        
        let vc = ArticleDetailsViewController(nibName: "ArticleDetailsViewController", bundle: nil)
        vc.article = article
        self.navigationController.pushViewController(vc, animated: true)
    }
    
}
