import Foundation
import RxSwift


final class NYTimesViewModel {
  
  private let fetchPeriodicForecastUseCase: FetchMostPopularArticlesUseCase
  var periodicForcasts : PopularArticlesResponse?
  var items: [Article] = [Article]()
  
  // MARK: - Init
  init( fetchPeriodicForecastUseCase: FetchMostPopularArticlesUseCase)
  {
    self.fetchPeriodicForecastUseCase = fetchPeriodicForecastUseCase
  }
  
  // Execute fetch periodic forecast use case
  func fetchMostPopularArticles(with FilterType : String , Period : Int ) -> Observable<Result<PopularArticlesResponse>>
  {
    return fetchPeriodicForecastUseCase.execute(with: FilterType , Period: Period ).do(onNext:  { result in
      switch result {
      case .success( let data , _ , _) :
        self.periodicForcasts = data
        if let forecasts = data.articles{
          self.items = forecasts
        }
        break
      default:
        break
      }
      
    })
  }
  
}
