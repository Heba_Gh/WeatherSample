import UIKit
import Kingfisher

class ArticleTableViewCell: UITableViewCell {
  
  @IBOutlet weak var userImage: UIImageView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var userType: UILabel!
    @IBOutlet weak var articleTitleLabel: UILabel!
  
    @IBOutlet weak var publishedDateLabel: UILabel!
    private var article : Article! {
    didSet
    {
        name.text = article.abstract
        userType.text = article.byline
        articleTitleLabel.text = article.Title
        publishedDateLabel.text = article.published_date
        if article.media.count > 0 {
            let url = article.media[0].mediaMetadata[1].url
            userImage.kf.indicatorType = .activity
            userImage.kf.setImage(with: URL(string: url) )
        }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func configure(article : Article) {
    self.article = article
  }
 
}
