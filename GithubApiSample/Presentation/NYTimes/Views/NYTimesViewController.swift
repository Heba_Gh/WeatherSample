import UIKit
import RxSwift

class NYTimesViewController: BaseViewController  {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var cityLabel: UILabel!
  
  var viewModel: NYTimesViewModel!
  let disposeBag = DisposeBag()
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    self.tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleTableViewCell")
      self.tableView.delegate = self
      self.tableView.dataSource = self
    fetchMostPopularArticles()
  }
  
  func fetchMostPopularArticles()  {
    let observable = viewModel.fetchMostPopularArticles(with: "all-sections", Period: 7)
    observable.subscribe(onNext: {
      result in
      switch result {
      case .loading:
        self.showFullScreenLoading()
        break
      case .failure(let error):
        self.finishFullScreenLoading()
        DispatchQueue.main.async {
          self.showFullScreenError(view: self.view, message: error?.message ?? "unknown error" ,  delegate: self)
        }
        break
      case .success(_, _ , _):
        self.finishFullScreenLoading()
        self.tableView.reloadData()
       
        break
      }
    }).disposed(by: disposeBag)
  }
}

// MARK: - UITableViewDataSource
extension NYTimesViewController: UITableViewDataSource{
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.items.count
  }
  

    
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell",
                                                   for: indexPath) as? ArticleTableViewCell else {
      
      return UITableViewCell()
    }
    
    cell.configure(article: viewModel.items[indexPath.row])
    return cell
  }
    

    

}
extension NYTimesViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        MainCoordinator.shared?.pushArticleDetails(article: viewModel.items[indexPath.row])
    }
}

// MARK: - Instance
extension NYTimesViewController {
  
  static func getInstance() -> NYTimesViewController {
    
    let nyTimesViewController = NYTimesViewController(nibName: "NYTimesViewController", bundle: nil)
    
    return nyTimesViewController
  }
}

// MARK: - Error delegate handeling
extension NYTimesViewController: CustomErrorViewDelegate
{
  func didTapRetry(){
    if isOnline{
    self.hideFullScreenError()
    fetchMostPopularArticles()
    }
  }
}
