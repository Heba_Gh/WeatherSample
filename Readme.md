#  NYTimes Most Popular Articles iOS App

This repository contains an iOS application that utilizes the NYTimes Most Popular Articles API to display a list of the most popular articles.

The project follows the MVVM design pattern and incorporates a layered clean architecture for better code organization and maintainability.

Reactive programming with RXSwift is used for handling asynchronous events, and Swinject is employed for dependency injection. 

Additionally, the coordinator design pattern is implemented for navigation and flow control.

# Requirements

- Xcode 12.0 or later
- Swift 5.0 or later
- iOS 11.0 or later

# Getting Started

To run the project on your local machine, follow these steps:

- Clone the repository to your local machine or download the ZIP file.
- Open the NYTimesMostPopularArticles.xcodeproj file using Xcode.
- Install any required dependencies using Cocoapods. Run the following command in the terminal:
- pod install
- Build and run the project using the Xcode simulator or by connecting a physical iOS device.


# Architecture

The project follows a layered clean architecture pattern, separating the concerns of different components. The architecture consists of the 
following layers:

Presentation Layer: Contains the user interface components, including View Controllers, Views, and View Models. This layer is responsible for
displaying data and handling user interactions. The MVVM design pattern is used to facilitate separation of concerns between the View and
ViewModel.

Domain Layer: Contains the business logic of the application. It defines the use cases and protocols that describe the operations and interactions
with the data.

Data Layer: Handles the data retrieval and storage. In this project, it interacts with the NYTimes Most Popular Articles API to fetch the required
data. The data layer implements the Repository pattern to abstract the data sources and provide a clean interface for the domain layer.

Network Layer: Manages the networking functionality and communicates with the API endpoints. It utilizes RXSwift for reactive programming and
asynchronous operations.

Dependency Injection Layer: Implements the dependency injection pattern using Swinject to provide loose coupling between different components of
the application.
It allows for easier unit testing and facilitates modularity.

Coordinator Layer: Handles the navigation and flow control within the application. 
It follows the coordinator design pattern, ensuring separation of concerns and improving the maintainability of the navigation logic.
Dependencies

# The project uses the following dependencies, managed by Cocoapods:

- RXSwift: For reactive programming and handling asynchronous events.
- Swinject: For dependency injection and managing object creation and dependencies.
- Alamofire: For network requests and communication with the NYTimes Most Popular Articles API.
- Kingfisher: For downloading and caching images from URLs.

All the required dependencies are specified in the Podfile included in the project.

# Contributing

If you'd like to contribute to this project, please follow these steps:

Fork the repository on GitHub.

Create a new branch from the master branch for your work.

Make your changes and improvements.

Test your changes to ensure they work as expected.

Commit and push your changes to your forked repository.

Create a pull request to merge your changes into the main repository.

Please ensure that your code follows the existing coding style and conventions. Also, include relevant documentation and tests with your changes.

# License

This project is licensed under the MIT License, which allows you to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the software. Refer to the LICENSE file for more details.

# Acknowledgements

The NYTimes for providing the Most Popular Articles API.

The open-source community for the various libraries and tools used in this project.



